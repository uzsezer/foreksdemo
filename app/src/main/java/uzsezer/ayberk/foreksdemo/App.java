package uzsezer.ayberk.foreksdemo;

import android.app.Application;
import android.content.Context;

/**
 * Created by ayberkuzsezer on 16/05/17.
 */

public class App extends Application {

    private AppComponent appComponent;


    @Override
    public void onCreate() {
        super.onCreate();

        injectDependencies();
    }

    public static App get(Context ctx){
        return (App) ctx.getApplicationContext();
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }

    private void injectDependencies(){
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        appComponent.inject(this);
    }
}
