package uzsezer.ayberk.foreksdemo.base;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by ayberkuzsezer on 16/05/17.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface BaseScope {
}
