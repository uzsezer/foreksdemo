package uzsezer.ayberk.foreksdemo.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.transition.Fade;
import android.widget.Toast;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import butterknife.ButterKnife;
import uzsezer.ayberk.foreksdemo.App;
import uzsezer.ayberk.foreksdemo.AppComponent;
import uzsezer.ayberk.foreksdemo.bus.ErrorEvent;
import uzsezer.ayberk.foreksdemo.service.ServiceConnector;

/**
 * Created by ayberkuzsezer on 16/05/17.
 */

public abstract class BaseActivity extends Activity {

    @Inject
    Bus bus;

    @Inject
    ServiceConnector serviceConnector;

    protected abstract int getLayoutId();

    protected abstract void injectDependencies(AppComponent appComponent);

    protected abstract void getIntentExtras();

    protected abstract void setUiProp();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getIntentExtras();

        setContentView(getLayoutId());

        injectDependencies(App.get(this).getAppComponent());

        ButterKnife.bind(this);

        setUiProp();

        setEnterAnimation();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try{
            bus.register(this);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            bus.unregister(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public ServiceConnector getServiceConnector(){
        return serviceConnector;
    }

    public Bus getBus(){
        return bus;
    }

    public void startActivity(Class T,Bundle bundle){
        Intent intent = new Intent(getApplicationContext(),T);

        if(bundle != null){
            intent.putExtras(bundle);
        }
        startActivity(intent);

    }

    private void setEnterAnimation(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Fade fade = new Fade();
            fade.setDuration(1000);
            getWindow().setEnterTransition(fade);
        }
    }

    @Subscribe
    public void onErrorReceive(ErrorEvent error){
        Toast.makeText(this, error.errorMessage, Toast.LENGTH_SHORT).show();
    }

}
