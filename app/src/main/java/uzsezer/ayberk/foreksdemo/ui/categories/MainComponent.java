package uzsezer.ayberk.foreksdemo.ui.categories;


import dagger.Component;
import uzsezer.ayberk.foreksdemo.AppComponent;
import uzsezer.ayberk.foreksdemo.base.BaseScope;

/**
 * Created by ayberkuzsezer on 16/05/17.
 *
 */
@BaseScope
@Component(
        dependencies = AppComponent.class
)
public interface MainComponent {
    void inject (MainActivity mainActivity);

}
