package uzsezer.ayberk.foreksdemo.ui.Splash;

import dagger.Component;
import uzsezer.ayberk.foreksdemo.AppComponent;
import uzsezer.ayberk.foreksdemo.base.BaseScope;

/**
 * Created by ayberkuzsezer on 16/05/17.
 */

@BaseScope
@Component(
        dependencies = AppComponent.class
)
public interface SplashComponent {
    void inject(SplashActivity splashActivity);
}
