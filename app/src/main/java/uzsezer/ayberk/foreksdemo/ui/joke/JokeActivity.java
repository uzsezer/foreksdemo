package uzsezer.ayberk.foreksdemo.ui.joke;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


import butterknife.BindView;
import uzsezer.ayberk.foreksdemo.AppComponent;
import uzsezer.ayberk.foreksdemo.R;
import uzsezer.ayberk.foreksdemo.base.BaseActivity;
import uzsezer.ayberk.foreksdemo.model.Joke;

/**
 * Created by ayberkuzsezer on 17/05/17.
 */

public class JokeActivity extends BaseActivity {

    private JokeComponent jokeComponent;

    private Joke jokeData;

    private final String BUNDLE_KEY_JOKE = "joke";

    @BindView(R.id.tvJokeContent)
    TextView tvJokeContent;

    @BindView(R.id.tvRepeatedWords)
    TextView tvRepeatedWords;

    @BindView(R.id.tvRepeatedCharacters)
    TextView tvRepeatedCharacters;

    @BindView(R.id.ivJokeImage)
    ImageView ivJokeImage;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_joke;
    }

    @Override
    protected void getIntentExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            jokeData = bundle.getParcelable(BUNDLE_KEY_JOKE);
        }
    }

    @Override
    protected void injectDependencies(AppComponent appComponent) {
        jokeComponent = DaggerJokeComponent.builder().appComponent(appComponent).build();
        jokeComponent.inject(this);
    }

    @Override
    protected void setUiProp() {
        Picasso.with(getApplicationContext()).load(jokeData.getIcon_url()).into(ivJokeImage);

        String text = jokeData.getValue();
        tvJokeContent.setText(text);
        tvRepeatedWords.setText(jokeData.repeatedWords(text));
        tvRepeatedCharacters.setText(jokeData.repeatedCharacters(text));
    }


}
