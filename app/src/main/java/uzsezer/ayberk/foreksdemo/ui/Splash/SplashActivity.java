package uzsezer.ayberk.foreksdemo.ui.Splash;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import uzsezer.ayberk.foreksdemo.AppComponent;
import uzsezer.ayberk.foreksdemo.R;
import uzsezer.ayberk.foreksdemo.base.BaseActivity;
import uzsezer.ayberk.foreksdemo.bus.ErrorEvent;
import uzsezer.ayberk.foreksdemo.model.Categories;
import uzsezer.ayberk.foreksdemo.ui.categories.MainActivity;

/**
 * Created by ayberkuzsezer on 16/05/17.
 */

public class SplashActivity extends BaseActivity {

    SplashComponent splashComponent;

    private final String BUNDLE_KEY_CATEGORIES ="categories";

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void getIntentExtras() {

    }

    @Override
    protected void injectDependencies(AppComponent appComponent) {
        splashComponent = DaggerSplashComponent.builder().appComponent(appComponent).build();
        splashComponent.inject(this);
    }

    @Override
    protected void setUiProp() {
        getServiceConnector().getCategories();
    }

    @Subscribe
    public void onResponse(Categories categories){
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_KEY_CATEGORIES,categories);
        startActivity(MainActivity.class,bundle);
        finish();
    }

    @Subscribe
    public void onErrorReceive(ErrorEvent error){
        Toast.makeText(this, error.errorMessage, Toast.LENGTH_LONG).show();
    }

}
