package uzsezer.ayberk.foreksdemo.ui.joke;

import dagger.Component;
import uzsezer.ayberk.foreksdemo.AppComponent;
import uzsezer.ayberk.foreksdemo.base.BaseScope;


/**
 * Created by ayberkuzsezer on 17/05/17.
 */

@BaseScope
@Component(
        dependencies = AppComponent.class
)
public interface JokeComponent {
    void inject(JokeActivity jokeActivity);
}
