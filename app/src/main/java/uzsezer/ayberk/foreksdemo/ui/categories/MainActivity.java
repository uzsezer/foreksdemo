package uzsezer.ayberk.foreksdemo.ui.categories;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.squareup.otto.Subscribe;

import butterknife.BindView;
import uzsezer.ayberk.foreksdemo.AppComponent;
import uzsezer.ayberk.foreksdemo.R;
import uzsezer.ayberk.foreksdemo.base.BaseActivity;
import uzsezer.ayberk.foreksdemo.model.Categories;
import uzsezer.ayberk.foreksdemo.model.Joke;
import uzsezer.ayberk.foreksdemo.ui.joke.JokeActivity;

public class MainActivity extends BaseActivity implements CategoriesAdapter.CategoryClickListener{

    private MainComponent mainComponent;

    private CategoriesAdapter categoriesAdapter;
    private Categories categories;

    private final String BUNDLE_KEY_CATEGORIES = "categories";
    private final String BUNDLE_KEY_JOKE ="joke";

    @BindView(R.id.rvCategories)
    RecyclerView rvCategories;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void getIntentExtras() {
        Bundle bundle =  getIntent().getExtras();

        if(bundle != null ){
            categories = bundle.getParcelable(BUNDLE_KEY_CATEGORIES);
        }
    }

    @Override
    protected void injectDependencies(AppComponent appComponent) {
        mainComponent = DaggerMainComponent.builder().appComponent(appComponent).build();
        mainComponent.inject(this);
    }

    @Override
    protected void setUiProp() {
        categoriesAdapter = new CategoriesAdapter(categories.getCategories());
        categoriesAdapter.setCategoryClickListener(this);
        rvCategories.setLayoutManager(new LinearLayoutManager(this));
        rvCategories.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        rvCategories.setAdapter(categoriesAdapter);
    }

    @Override
    public void onCategoryClicked(String category) {
        getServiceConnector().getJoke(category);
    }

    @Subscribe
    public void onResponse(Joke response){
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_KEY_JOKE,response);
        startActivity(JokeActivity.class,bundle);
    }
}
