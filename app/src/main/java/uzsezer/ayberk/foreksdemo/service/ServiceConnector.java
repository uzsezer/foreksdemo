package uzsezer.ayberk.foreksdemo.service;

import com.squareup.otto.Bus;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uzsezer.ayberk.foreksdemo.bus.ErrorEvent;
import uzsezer.ayberk.foreksdemo.model.Categories;
import uzsezer.ayberk.foreksdemo.model.Joke;

/**
 * Created by ayberkuzsezer on 16/05/17.
 */

public class ServiceConnector {

    private API api;
    private Bus bus;

    @Inject
    public ServiceConnector(API api, Bus bus) {
        this.api = api;
        this.bus = bus;

    }

    public void getCategories() {

        api.getCategories().enqueue(new Callback<ArrayList<String>>() {
            @Override
            public void onResponse(Call<ArrayList<String>> call, Response<ArrayList<String>> response) {
                bus.post(new Categories(response.body()));
            }

            @Override
            public void onFailure(Call<ArrayList<String>> call, Throwable t) {
                bus.post(new ErrorEvent("Hata oluştu.Lütfen internet bağlantınızı kontrol ediniz"));
            }
        });
    }

    public void getJoke(String category) {
        api.getJoke(category).enqueue(new Callback<Joke>() {
            @Override
            public void onResponse(Call<Joke> call, Response<Joke> response) {
                bus.post(response.body());
            }

            @Override
            public void onFailure(Call<Joke> call, Throwable t) {
                bus.post(new ErrorEvent("Hata oluştu.Lütfen internet bağlantınızı kontrol ediniz"));
            }
        });
    }


}
