package uzsezer.ayberk.foreksdemo.service;


import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uzsezer.ayberk.foreksdemo.BuildConfig;

/**
 * Created by ayberkuzsezer on 16/05/17.
 */

@Module
public class ApiModule {

    public static final HttpUrl API_BASE_URL = HttpUrl.parse("https://api.chucknorris.io/");


    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor){
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideLogginInterceptor(){
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                if (BuildConfig.DEBUG){
                    Log.v("OkHttp",message);
                }
            }
        });
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    @Provides
    @Singleton
    Gson provideGson(){
        return new GsonBuilder()
                .create();
    }

    @Provides
    @Singleton
    HttpUrl provideBaseUrl(){
        return API_BASE_URL;
    }


    @Provides
    @Singleton
    Retrofit provideRetrofit(HttpUrl baseUrl, OkHttpClient okHttpClient,Gson gson){
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Provides
    @Singleton
    API provideAPI(Retrofit retrofit){
        return retrofit.create(API.class);
    }

    @Provides
    @Singleton
    Bus provideBus(){
        return new Bus();
    }

    @Provides
    @Singleton
    public ServiceConnector provideServiceConnector(API api,Bus bus){
        return new ServiceConnector(api,bus);
    }


}
