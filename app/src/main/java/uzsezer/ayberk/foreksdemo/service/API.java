package uzsezer.ayberk.foreksdemo.service;

import java.util.ArrayList;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import uzsezer.ayberk.foreksdemo.model.Joke;

/**
 * Created by ayberkuzsezer on 16/05/17.
 */

public interface API {

    @GET("/jokes/categories")
    Call<ArrayList<String>> getCategories();

    @GET("/jokes/random?category=category")
    Call<Joke> getJoke(@Query("category") String category);


}
