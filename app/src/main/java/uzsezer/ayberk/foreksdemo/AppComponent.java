package uzsezer.ayberk.foreksdemo;

import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import uzsezer.ayberk.foreksdemo.service.API;
import uzsezer.ayberk.foreksdemo.service.ApiModule;
import uzsezer.ayberk.foreksdemo.service.ServiceConnector;

/**
 * Created by ayberkuzsezer on 16/05/17.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                ApiModule.class
        }
)

public interface AppComponent {
    void inject(App app);

    ServiceConnector getServiceConnector();
    Bus getBus();
}
