package uzsezer.ayberk.foreksdemo.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by ayberkuzsezer on 16/05/17.
 */

public class Joke implements Parcelable {

    private List<String> category = null;

    private String icon_url;

    private String id;

    private String value;

    private String url;

    public Joke() {
    }

    protected Joke(Parcel in) {
        category = in.createStringArrayList();
        icon_url = in.readString();
        id = in.readString();
        value = in.readString();
        url = in.readString();
    }

    public static final Creator<Joke> CREATOR = new Creator<Joke>() {
        @Override
        public Joke createFromParcel(Parcel in) {
            return new Joke(in);
        }

        @Override
        public Joke[] newArray(int size) {
            return new Joke[size];
        }
    };

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public List<String> getCategory() {
        return category;
    }

    public void setCategory(List<String> category) {
        this.category = category;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(category);
        dest.writeString(icon_url);
        dest.writeString(id);
        dest.writeString(value);
        dest.writeString(url);
    }

    public String repeatedCharacters(String value){

        String[] characters = value.toLowerCase(Locale.ENGLISH).replaceAll("[^A-Za-z0-9]","").split("");
        LinkedHashMap<String,Integer> lhmCharacters = new LinkedHashMap<>();

        for (String character:characters) {

            if(character.equals("")){
                continue;
            }

            int count = 1;
            if(lhmCharacters.get(character) != null){
                count = lhmCharacters.get(character) + 1;
            }
            lhmCharacters.put(character,count);
        }

        Arrays.sort(lhmCharacters.keySet().toArray());

        return lhmCharacters.toString().replaceAll("[{}]","");
    }

    public String repeatedWords(String value){

        String[] words = value.replaceAll("[-+.\\^\":,!?]","").toLowerCase(Locale.ENGLISH).split(" ");
        LinkedHashMap<String,Integer>  lhmWords = new LinkedHashMap<>();

        for (String word: words) {
            int count = 1;
            if(lhmWords.get(word) != null){
                count = lhmWords.get(word) + 1 ;
            }
            lhmWords.put(word,count);
        }

        return lhmWords.toString().replaceAll("[{}]","");
    }
}
