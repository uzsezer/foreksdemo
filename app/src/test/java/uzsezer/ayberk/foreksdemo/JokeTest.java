package uzsezer.ayberk.foreksdemo;

import org.junit.Before;
import org.junit.Test;

import uzsezer.ayberk.foreksdemo.model.Joke;

import static junit.framework.Assert.assertEquals;

/**
 * Created by tmobuser on 18/05/17.
 */

public class JokeTest {

    private Joke joke;

    @Before
    public void setJoke(){
       joke = new Joke();
    }

    @Test
    public void checkRepeatedWords() throws Exception{
        String content = "When Chuck Norris goes to out to eat, he orders a whole chicken, but he only eats its soul.";
        String expected = "when=1, chuck=1, norris=1, goes=1, to=2, out=1, eat=1, he=2, orders=1, a=1, whole=1, chicken=1, but=1, only=1, eats=1, its=1, soul=1";
        assertEquals(expected,joke.repeatedWords(content));
    }

    @Test
    public void checkRepeatedCharacters() throws Exception{
        String content = "Chuck Norris ordered a Big Mac at Burger King,and got one.";
        String expected = "c=3, h=1, u=2, k=2, n=4, o=4, r=6, i=3, s=1, d=3, e=4, a=4, b=2, g=4, m=1, t=2";
        assertEquals(expected,joke.repeatedCharacters(content));

    }
}
